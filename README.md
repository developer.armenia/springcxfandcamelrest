for Apache CXF Rest API I have created the apache-cxf-api Spring boot project.
for Apache Camel Rest API I have created the apache-camel-api Spring boot project.

How to run project
its needed java 11 and maven 3.3.9.
when you have java nad maven in your classpath you can at buid the project 
how to buid the project:

apache-camel-api>mvn package
apache-cxf-api>mvn package

after successfully build you can run the API with following command
apache-camel-api>mvn spring-boot:run
apache-cxf-api>mvn spring-boot:run

how we can test the project?
for example with postman

URL for Apache CXF Rest API (http://localhost:8081/api/reverse/{anymessage})
URL for Apache Camel Rest API (http://localhost:8080/camel/engine/{anymessage})

---------------------------------------------------------
The Apache CXF Rest API have one API api/reverse which get mesage and revert it
The Apache Camel Rest API have one API api/engine which get mesage and send it to Apache CXF Rest API


