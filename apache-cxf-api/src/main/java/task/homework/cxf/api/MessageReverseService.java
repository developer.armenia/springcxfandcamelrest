package task.homework.cxf.api;

import org.springframework.beans.factory.annotation.Autowired;
import task.homework.cxf.service.ReverseService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("reverse")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
public class MessageReverseService {

	@Autowired
	public ReverseService reverseService;

	@Path("{message}")
	@GET
	public Response getById(@PathParam("message") String message){
		return Response.ok(reverseService.reverseMessage(message)).build();
	}
}
