package task.homework.cxf.service;

import org.springframework.stereotype.Service;

@Service
public class ReverseService {

    public String reverseMessage(String message){
        String reversedString="";
        char ch;

        for (int i=0; i<message.length(); i++)
        {
            ch = message.charAt(i);
            reversedString = ch + reversedString;
        }
        return reversedString;
    }
}
