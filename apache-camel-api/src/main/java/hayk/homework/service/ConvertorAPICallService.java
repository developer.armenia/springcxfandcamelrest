package hayk.homework.service;

import hayk.homework.payload.MessageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
public class ConvertorAPICallService {
    @Autowired
    RestTemplate restTemplate;

    public String getConvertedMessage(String msg){

        MessageBean messageBean = new MessageBean();
        messageBean.setMessage(msg);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        HttpEntity<MessageBean> entity = new HttpEntity<MessageBean>(messageBean,headers);

        String body = restTemplate.exchange(
                "http://localhost:8081/api/reverse/"+msg, HttpMethod.GET, entity, String.class).getBody();
        return body;

    }
}
