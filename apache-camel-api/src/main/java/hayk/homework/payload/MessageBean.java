package hayk.homework.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "EXPORT_XML")
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageBean {

    private int id;

    private String type;

    private String message;

}
