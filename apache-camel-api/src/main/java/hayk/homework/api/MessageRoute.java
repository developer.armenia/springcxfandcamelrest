package hayk.homework.api;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;

import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageRoute extends RouteBuilder {

    @Override
    public void configure() throws JAXBException {
        log.info(">>start config MessageRoute.configure() ");
        restConfiguration().host("localhost").port(8081).bindingMode(RestBindingMode.json);
        rest("/engine")
                .produces("application/json")
                .consumes("application/json")
                .get("/{msg}")
                .param().name("msg").endParam()
                .to("bean:convertorAPICallService?method=getConvertedMessage(${header.msg})");

    }


}
